package me.zattair.updater.update;

import me.zattair.updater.request.Requester;

import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class Updater {

    private final String FOLDER_PATH;
    private final String SERVER_URL;

    public final Requester requester;
    public final Hasher hasher;

    private final ClientFilesCheck clientFilesCheck;
    private final ServerFilesCheck serverFilesCheck;

    public Updater(String folderPath, String serverURL) {
        this.FOLDER_PATH = folderPath;
        this.SERVER_URL = serverURL;

        this.requester = new Requester(serverURL);
        this.hasher = new Hasher();

        this.clientFilesCheck = new ClientFilesCheck(this, FOLDER_PATH);
        this.serverFilesCheck = new ServerFilesCheck(this, SERVER_URL);
    }

    public boolean start() {
        HashMap<String, String> clientHashs = clientFilesCheck.getFilesHashMap();
        HashMap<String, String> serverHashs = serverFilesCheck.getFilesHashMap();

        for(String key : serverHashs.keySet()) {
            if(clientHashs.containsKey(key)) {
                if(!(clientHashs.get(key).equals(serverHashs.get(key)))) {
                    System.out.println("Need to Download " + key);
                    try {
                        removeAndDownloadFile(key);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            } else {
                System.out.println("Need to Download " + key);
                try {
                    downloadFile(key);
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }

        }
        return true;
    }

    private void removeAndDownloadFile(String filePath) throws IOException {
        System.out.println(FOLDER_PATH + filePath);
        clientFilesCheck.removeFile(FOLDER_PATH + "/" + filePath);
        ReadableByteChannel readableByteChannel = Channels.newChannel(this.requester.readFile(filePath));
        FileOutputStream fileOutputStream = new FileOutputStream(FOLDER_PATH + "/" + filePath);
        FileChannel fileChannel = fileOutputStream.getChannel();
        fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
    }

    private void downloadFile(String filePath) throws IOException {
        File newFile = new File(FOLDER_PATH + "/" + filePath);
        newFile.getParentFile().mkdirs();
        ReadableByteChannel readableByteChannel = Channels.newChannel(this.requester.readFile(filePath));
        FileOutputStream fileOutputStream = new FileOutputStream(FOLDER_PATH + "/" + filePath);
        FileChannel fileChannel = fileOutputStream.getChannel();
        fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
    }

}
