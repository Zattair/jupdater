package me.zattair.updater.update;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

public class ClientFilesCheck {

    private final Updater updater;
    private ArrayList<String> ignoreFiles;

    private File baseFolder;

    private HashMap<String, String> fileHash = new HashMap<String, String>();

    public ClientFilesCheck(Updater updater, String basePath) {
        this.updater = updater;
        this.ignoreFiles = getIgnoredFiles();
        this.baseFolder = new File(basePath);
    }


    public HashMap<String, String> getFilesHashMap() {
        fileHash = new HashMap<String, String>();;
        if(!this.baseFolder.isDirectory()) {
            // TODO ERROR FOLDER PROVIDE DON'T EXIST
            return null;
        }
        for(File f : this.baseFolder.listFiles()) {
            if(!this.ignoreFiles.contains(f.getName())) {
                try {
                    if(f.isDirectory()) {
                        listFiles(f);
                    } else {
                        //System.out.println(this.baseFolder.toURI().relativize(f.toURI()).getPath());
                        fileHash.put(this.baseFolder.toURI().relativize(f.toURI()).getPath(), updater.hasher.getFileChecksum(f, MessageDigest.getInstance("SHA-256")));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        }
        return this.fileHash;
    }

    private ArrayList<String> getIgnoredFiles() {
        try {
            return updater.requester.getTextContent(".ignore");
        } catch (IOException e) {
            // TODO CAN'T GET FILE FROM SERVER: ERROR AND CONTINUE OR STOP THE UPDATER AND SEND AN ERROR ?
            //System.out.println("Can't get .ignore file from the server!");
        }
        return null;
    }

    private void listFiles(File file) {
        for(File f : file.listFiles()) {
            if(f.isDirectory()) {
                listFiles(f);
            } else {
                try {
                    //System.out.println(this.baseFolder.toURI().relativize(f.toURI()).getPath());
                    fileHash.put(this.baseFolder.toURI().relativize(f.toURI()).getPath(), updater.hasher.getFileChecksum(f, MessageDigest.getInstance("SHA-256")));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void removeFile(String filePath) {
        File file = new File(filePath);
        file.delete();
    }

}
