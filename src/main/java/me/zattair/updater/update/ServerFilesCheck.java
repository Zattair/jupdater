package me.zattair.updater.update;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

public class ServerFilesCheck {

    private final Updater updater;

    private final String SERVER_URL;

    private HashMap<String, String> fileHash = new HashMap<>();

    public ServerFilesCheck(Updater updater, String serverURL) {
        this.updater = updater;
        this.SERVER_URL = serverURL;
    }

    public HashMap<String, String> getFilesHashMap() {
        fileHash = new HashMap<>();
        try {
            ArrayList<String> files = getFilesPath();
            for(int i = 0; i < files.size(); i++) {
                if(files.get(i).isEmpty() || files.get(i).startsWith("#")) {

                } else {
                    fileHash.put(files.get(i), updater.hasher.getFileChecksum(this.SERVER_URL + "/" + files.get(i), MessageDigest.getInstance("SHA-256")));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return fileHash;
    }

    private ArrayList<String> getFilesPath() throws IOException {
        return updater.requester.getTextContent(".updater.download");
    }



}
