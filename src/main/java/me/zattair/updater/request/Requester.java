package me.zattair.updater.request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class Requester {

    private final String serverHostname;
    public Requester() {
        this.serverHostname = "";
    }

    public Requester(String serverHostname) {
        this.serverHostname = serverHostname;
    }

    public Object sendRequest(String request) throws IOException {
        return send(request, "GET");
    }

    public ArrayList<String> getTextContent(String request) throws IOException {
        return (ArrayList<String>) send(request, "GET");
    }

    public InputStream readFile(String request) throws IOException {
        URL url;
        if(serverHostname.isEmpty()) {
            url = new URL("https://" + request);
        } else {
            url = new URL("https://" + this.serverHostname + (this.serverHostname.endsWith("/") ? "" : "/") + request);
        }
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setInstanceFollowRedirects(true);
        connection.setRequestMethod("GET");
        connection.addRequestProperty("User-Agent", "Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36");

        return connection.getInputStream();
    }

    private Object send(String request, String method) throws IOException {
        URL url;
        if(serverHostname.isEmpty()) {
            url = new URL("https://" + request);
        } else {
            url = new URL("https://" + this.serverHostname + (this.serverHostname.endsWith("/") ? "" : "/") + request);
        }

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setInstanceFollowRedirects(true);
        connection.setRequestMethod(method);
        connection.addRequestProperty("User-Agent", "Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36");

        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        ArrayList<String> res = new ArrayList<String>();
        String currentLine;

        while((currentLine = br.readLine()) != null) {
            res.add(currentLine);
        }
        return res;
    }

}
