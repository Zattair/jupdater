# JavaUpdater


[![Buy me a Coffee](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/Zattairr)


## Server Setup
JavaUpdater Server Side (JUSS) is straightforward to setup. All you need, is to create a ```.updater.download``` file on your preconfigured HTTP Server (HTTPS download will be possible in the next version). It will contain relative server paths for the files that the Client Side (JUCS) needs to download.

Example config for ```.updater.download```:
```
# Loading Executable
executable.jar

# Loading Librairies now
libs/JavaUpdater.jar
config/jupdater.conf
```

If there is any Client custom files/folders that doesn't need to be checked/download (like logs folder) you can create a ```.ignore``` file, on the Server, with relative paths for all files/folders.

Example config for ```.ignore```:
```
saves
logs
resourcepacks
```

## Using JavaUpdater
To setup JavaUpdater's Client Side (JUCS), just download the latest JavaUpdater jar file, add it as a Library into your project.

Here is an example of a default JUCS structure:
```java
Updater updater = new Updater(CLIENT_MAIN_FOLDER, SERVER_URL);
// SERVER_URL example: "static.zattair.me/launcher"
if(updater.start()) {
    // The client as all the files needed, you can proceed.
} else {
    // Something went wrong...
    /*
    * ###################################################################################
    * # SOME IMPROVEMENTS WILL BE MADE IN THIS PART TO RETURN AN ERROR WITH MORE INFOS. #
    * ###################################################################################
    */
}
```

## Checksum & File Integrity
When starting the updater, it will go through each file and make a checksum to be sure that all files on the client folder are the same as the ones on the server. If any differences are noticed, the Client will try to delete and download the file again from the server.

## Disclaimers & Improvements
This is the first SNAPSHOT, there may be some bugs and issues, if you encounter one of these, please open an issue on GitLab!

In future versions, we want to add more download status information and the possibility to configure more settings (using HTTPS, using others checksum methods)




